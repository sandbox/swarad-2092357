About
=====
Integrates the Sidr jquery library into Drupal.

Current Options
---------------
Allows you to have a drupal menu as a sidr responsive menu.

About Sidr
----------------

Library available at https://github.com/artberri/sidr-package
See also http://www.berriart.com/sidr

- Easy to use API to convert an HTML markup into a side menu.
- There are two themes (two stylesheets) included with the plugin, 
  a dark one and a light one. You can use them, create a new one or 
  override them with your own styles.
- Free to use under the MIT license


Installation
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [Sidr Library](https://github.com/artberri/sidr-package)

Steps
------------------
1. Go to https://github.com/artberri/sidr-package/releases 
   and download the latest sidr package.
2. Extract the zip in sites/all/libraries.
3. Rename the directory to sidr from sidr-package-x.x.x
4. Ensure the directory sidr is placed properly so that the library exists at
   sites/all/libraries/sidr/jquery.sidr.min.js
5. Enable the module.
6. Go to admin/config/sidr and select the menu which you want to show when
   the site is viewed on mobile devices.
7. Choose wether you want to have a Link text or Link image (default or add
   a custom local image path) as the anchor for opening the sidr menu.
8. Save.

After the above you should be able to see the menu you selected as a sidr menu.

NOTE: Right now I have just intergated the sidr library as a responsive menu, 
but the entire library is already part of this module, also is part of the 
module the dark and light themes. So you can use it to extend this module's 
functionality and have any other usage of library working. 

Refer the documentation for more info 
http://www.berriart.com/sidr/#documentation.
