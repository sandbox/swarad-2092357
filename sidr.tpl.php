<?php
/**
 * @file
 * Tpl file for sidr menu html.
 */
?>
<div class="sidr <?php print $replace_with_image;?>" id ="sidr">
  <?php print $menu; ?>
</div>
<?php if($flag == TRUE): ?>
<a id="open-sidr"><img src = "<?php print $image_path;?>"/></a>
<?php else: ?>
<a id="open-sidr"><?php print $link_text;?></a>
<?php endif; ?>
