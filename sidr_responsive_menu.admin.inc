<?php

/**
 * @file
 * Contains form builder functions for module configurations.
 */

/**
 * Admin form builder for sidr menu configurations.
 */
function sidr_responsive_menu_configurations() {
  $form = array();
  // Gather all the menus in one place.
  $menus = menu_get_menus();
  $form['sidr_responsive_menu_select_menu'] = array(
    '#type' => 'select',
    '#title' => t('Select the sidr menu.'),
    '#options' => $menus,
    '#default_value' => variable_get('sidr_responsive_menu_select_menu', 'main-menu'),
    '#description' => t('Select the main primary navigation of the site, which you want to function as sidr menu. Usually this is the main menu.'),
  );
  $form['sidr_responsive_menu_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link text.'),
    '#default_value' => variable_get('sidr_responsive_menu_link_text', 'Menu'),
    '#description' => t('The link text to use in the anchor tag.'),
  );
  $form['sidr_responsive_menu_image_or_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use an image instead of link text.'),
    '#default_value' => variable_get('sidr_responsive_menu_image_or_text', 1),
    '#description' => t('Use an image instead of the link text above.'),
  );
  $form['sidr_responsive_menu_file_path'] = array(
    '#type' => 'managed_file',
    '#title' => t('Choose custom image.'),
    '#default_value' => variable_get('sidr_responsive_menu_file_path', ''),
    '#upload_validators' => array(
      'file_validate_extensions' => array('png jpg svg gif jpeg'),
    ),
    '#upload_location' => 'public://sidr_menu_images',
    '#description' => t('Upload a custom image and use that as menu icon.'),
  );
  return system_settings_form($form);
}
