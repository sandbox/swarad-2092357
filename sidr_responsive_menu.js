/**
* @file
* JS file for sidr, initilaizes the behaviours.
*/

(function ($) {
  Drupal.behaviors.sidr_responsive_menu = {
    attach: function (context, settings) {
      // Initialize.
      $("#open-sidr").sidr();
      // Check for width of window.
      var width = $(window).width();
      if (width < 480) {
        $("#open-sidr").show();
      }
      else {
        $("#open-sidr").hide();
      }
      
      $(window).resize(function(){
        var width = $(window).width();
        if (width < 480) {
          $("#open-sidr").show();
        }
        else {
          $("#open-sidr").hide();
        }
      });
    }
  };
}(jQuery));
